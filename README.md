# File Uploader by Uppy

Upload and manage files using the [Uppy](https://uppy.io/) JavaScript file uploader.

## Supported Uppy Plugins

- [Dashboard](https://uppy.io/docs/dashboard/) for the UI.
- [Image Editor](https://uppy.io/docs/image-editor/) for inline image editing before uploading.
- [XHR](https://uppy.io/docs/xhr-upload/) for the Client-to-Drupal storage method.
- [Internationalisation](https://uppy.io/docs/locales/) for UI translations.

## Dependencies

- [File Uploader](https://www.drupal.org/project/file_uploader)

## Usage

1. Download and install the `drupal/file_uploader_uppy` module. Recommended install method is composer:
   ```
   composer require drupal/file_uploader_uppy
   ```
2. Go to the "Manage form display" tab of the entity type.
3. Select the "File Uploader by Uppy" widget on the file field.
4. Configure and save changes.
